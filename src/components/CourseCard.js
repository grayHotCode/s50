
import {useState} from 'react'
import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'


export default function CourseCard({courseProp}){

	console.log(courseProp)

// Object destructuring
	const {name, description, price, _id} = courseProp
	// Symtax: const {properties} = coursePropname 

//Array destructuring
// count(initial value) setCount(function)
// const [count, setCount] = useState(0)
// Syntax: const[getter, setter] = useState(initialValue)

//Hook used is useState - to store the state

// function enroll(){
// 	setCount(count + 1);
// 	console.log('Enrollees' + count);

// 	if(count == 30){
// 		alert('No more seats');
// 	}
// }


	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className= "btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
		
	)
}













