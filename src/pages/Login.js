

import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'
	// with the latest version Navigate instead of Redirect
// import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'


export default function Login(){

	// Allows us to consume the user context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [password, setPassword] = useState('')
	const [email, setEmail] = useState('')

	// State to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)

	//check if values are succesfully binded 
	console.log(email)
	console.log(password)

	// 
	function authenticate(e){

		// Prevents page redirection via form submission
		e.preventDefault();


		fetch('http://localhost:4000/users/login',{
				method: 'POST',
				headers:{
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successfull!',
					icon: 'success',
					text: 'Welcome to Zuitt'
				})
			}else{
				Swal.fire({
					title: 'Authentication Failed!',
					icon: 'error',
					text: 'Check your login details'
				})
			}
		})


		// Set the email of the user in the local storage
		// Syntax: localStorage.setItem('propertyName', value);
		// localStorage.setItem('email', email);

		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		// Clears the input field
		setEmail('');
		setPassword('');

	}

	const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details', {
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data =>{
				console.log(data)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
	}



	useEffect(() => {
		if(email !== '' && password !== ''){

			setIsActive(true)

		}else{

			setIsActive(false)

		}

	}, [email, password])

	return(
		// kung saan mo gusto mapunta si user after login
		(user.id !== null) ?
			<Redirect to="/courses" />
		:	
		<Form onSubmit= {e=>authenticate(e)}>

			<Form.Text> Log In</Form.Text>
			<Form.Group>

				<Form.Label> Email Address:</Form.Label>			
				<Form.Control 
					type= 'email'
					placeholder= 'Please enter your email here'
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className= "text-muted">
					Well never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId = "password1">
				<Form.Label>Password: </Form.Label>
				<Form.Control 
					type='password'
					placeholder='Please input your password here'
					value = {password}
					onChange = {e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>


			{/*? if else :*/}
			{isActive ? 
				<Button variant='success' type= 'submit' id='submitBtn'>
							Submit
				</Button>

				:
				<Button variant='secondary' type= 'submit' id='submitBtn' disabled>
							Submit
				</Button>
			}

		</Form>


		)
}

// Ternary operator with the Button Tag
// ?- if -- if<Button isActive, the color will be primary and it is enabled>
// :- else -- <Button will be color danger and will be disabled>









