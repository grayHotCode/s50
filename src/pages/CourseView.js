
import {useState, useEffect, useContext} from 'react'
import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import {useParams, useHistory, Link} from 'react-router-dom'
// useHistory for v5 down in react-router-dom
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function CourseView (){

	// useParams hook allows us to retrieve the courseId passed via the URL.
	const {courseId} = useParams()

	const {user} = useContext(UserContext)

	// useHistory or useNavigate hook allows us to redirect user to a different page
	const history = useHistory()

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)


	const enroll = (courseId) => {
		fetch(`http://localhost:4000/users/enroll`, {
			method: 'POST',
			headers : {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: user.id
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Enrolled Successfully!',
					icon: 'success',
					text: 'You have successfully enrolled in this course'
				})

				history.push("/courses")

			} else {
				Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}

	useEffect(() => {
		console.log(courseId)

		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}, [courseId])



	return(
		<Container className = "mt-5">
			<Row>
				<Col lg = {{span: 6, offset: 3}}>
					<Card>
						<Card.Body className = "text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text> Php {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>5:30pm to 9:30pm</Card.Text>
							{user.id !== null ?
								<Button variant = "primary" onClick = {() => enroll(courseId)}>Enroll</Button>
								:
								<Link className = "btn btn-danger btn-block" to = "/login">Log in to Enroll</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}












// import {useState, useEffect, useContext} from 'react'
// import {Container, Row, Col, Card, Button} from 'react-bootstrap'
// // for getting id
// // useHistory for v5 down in react-router-dom
// // useNavigate if v6 up in react-router-dom
// import {useParams, useHistory, Link} from 'react-router-dom'
// import UserContext from '../UserContext'
// import Swal from 'sweetalert2'

// export default function CourseView(){

// 	// Hooks from react-router-dom
// 	// allows us to retrieve the courseId via the URL.
// 	const {courseId} = useParams()

// 	const {user} = useContext(useContext)

// 	// use to redirect user 
// 	const history = useHistory()

// 	const [name, setName] = useState('')
// 	const [description, setDescription] =  useState('')
// 	const [price, setPrice] = useState(0)

// 	const enroll = (courseId, userId) => {

// 		fetch(`http://localhost:4000/users/enroll`, {

// 			method: 'POST',
// 			headers: {
// 				"Content-Type": "application/json",
// 				Authorization: `Bearer ${localStorage.getItem("token")}`
// 			},
// 			body: JSON.stringify({
// 				courseId: courseId,
// 				userId: userId
// 			})
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data)

// 			if(data === true){
// 					Swal.fire({
// 						title: 'Enrolled Successfully',
// 						icon: 'success',
// 						text: 'You have success enrolled'
// 					})

// 				history.push("/courses")
					
// 			}else{
// 				Swal.fire({
// 						title: 'Something went wrong',
// 						icon: 'error',
// 						text: 'Please try again'
// 					})
// 			}
// 		})

// 	}


// 	useEffect(() =>{

// 		console.log(courseId)

// 		fetch(`http://localhost:4000/courses/${courseId}`)
// 		.then(res => res.json())
// 		.then(data =>{
// 			console.log(data)

// 			setName(data.name)
// 			setDescription(data.description)
// 			setPrice(data.price)
// 		})

// 	}, [courseId])


// // Template for single course viewing
// 	return(

// 		<Container className = "mt-5">
// 			<Row>
// 				<Col lg={{span: 6, offset: 3}}>
// 					<Card>
// 						<Card.Body className="text-center">
// 							<Card.Title>{name}</Card.Title>
// 							<Card.Subtitle>Description: </Card.Subtitle>
// 							<Card.Text>{description}</Card.Text>
// 						    <Card.Subtitle>Price:</Card.Subtitle>
// 						    <Card.Text>Php {price}</Card.Text>
// 						    <Card.Subtitle>Class Schedule</Card.Subtitle>
// 						    <Card.Text>5:30pm t0 9:30pm</Card.Text>

// 						    {user.id !== null?
// 						    	<Button variant= "primary" onClick={() => enroll(courseId)}>Enroll</Button>
// 						    	:
// 						    	<Link className= "btn btn-danger btn-block" to = "/login" > Log In to enroll
						    		
// 						    	</Link>
// 						    }

// 						</Card.Body>
// 					</Card>
// 				</Col>
// 			</Row>
// 		</Container>	

// 		)

// }